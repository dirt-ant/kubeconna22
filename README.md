# kubeconna22

## Getting started

Welcome to the KubeCon NA 2022 Challenge hosted on CodeChallenge.dev. `Were so glad your here`.

## Level 1

Create a merge request to enable SLSA 2 metadata collection

Add the [snippet found here](https://gitlab.com/gitlab-code-challenge/kubeconna22/-/snippets/2427333) to the top of the .gitlab-ci.yml in a merge request.

## Level 2

Add dependancy scanning to the project

Review the documentation on [GitLab Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#configuration). Once you understand how it works, make a merge request to the GitLab CI/CD pipeline to add dependency scanning on every new commit.

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to: The Ruby on Rails backend, the Vue-based frontend, the Go-based services like the GitLab Runner and Gitaly, and the documentation for all of those things and more.
